<%-- 
    Document   : criarConta
    Created on : 28/09/2019, 17:10:54
    Author     : lucas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="robots" content="index, follow"/>

        <title>Embuste</title>

        <!-- Simbols -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Style -->
		<link rel="stylesheet" href="../css/reset.css">
		<link rel="stylesheet" href="../css/styles.css">

    </head>

    <body>
         <div class="pano-menu-mobile"></div>

        <nav id="nav1" class="engloba-nav-bar transition">
            <div class="logo">
                <img alt="pay now" src="../img/logo.png">
            </div>
            <ul class="nav-itens">
                <li>
                    <a class="nav-item" href="http://localhost:8080/TrabalhoWeb2/telasLogin/login.jsp">
                        Login
                    </a>
                </li>
                <li >
                    <a class="nav-item nav-active" href="">
                       Criar conta
                    </a>
                </li>
            </ul>
        </nav>


        <header id="nav2" class="top-header transition" >
            <div class="logo">
                <img alt="pay now" src="../img/logo.png">
            </div>
            <div class="button-menu">
                <i class="open fas fa-bars"></i>
                <i class="close fas fa-times"></i>
            </div>
        </header>

        <section class="section-banner">
            <div class="container">
                <div class="banner">
                    <h1 class="title-usuario">Criar conta</h1>
                </div>
            </div>
        </section>

        <!--Estatisticas-->


        <section class="section">
            <div class="container">
                <h2>Informações</h2>
                <div class="atendimento-info">
                    <div class="campo-input">
                        <div class="data-hora">
                            <div class="">
                                Nome:<br>
                                <input type="text" name="" value="">
                            </div>
                            <div class="">
                                CPF:<br>
                                <input type="text" name="" value="">
                            </div>
                        </div>
                    </div>
                    <div class="campo-input">
                        email:<br>
                        <input id="input-maior" type="text" name="" value="">
                    </div>
                    <div class="campo-input">
                        <div class="data-hora">
                            <div class="">
                                Telefone<br>
                                <input type="text" name="" value="">
                            </div>
                            <div class="">
                                CEP<br>
                                <input type="text" name="" value="">
                            </div>

                        </div>
                    </div>
                    <div class="campo-input">
                        <div class="data-hora">
                            <div class="">
                                Rua<br>
                                <input type="text" name="" value="">
                            </div>
                            <div class="">
                                Número<br>
                                <input type="text" name="" value="">
                            </div>

                        </div>
                    </div>
                    <div class="campo-input">
                        <div class="data-hora">
                            <div class="">
                                Complemento<br>
                                <input type="text" name="" value="">
                            </div>
                            <div class="">
                                Bairro<br>
                                <input type="text" name="" value="">
                            </div>

                        </div>
                    </div>
                    <div class="campo-input">
                        <div class="data-hora">
                            <div class="">
                                Cidade<br>
                                <input type="text" name="" value="">
                            </div>
                            <div class="">
                                Estado<br>
                                <input type="text" name="" value="">
                            </div>
                        </div>
                    </div>

                    <section class="section">
                        <div class="container">
                            <div class="novo">
                                <a class="btn btn-success btn-xs bottt"  href="#" data-toggle="modal" data-target="#delete-modal">Criar</a>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </section>


        <footer class="footer">
            <div class="container">
                <img alt="embuste" src="../img/logo.png">
                <p>Criado com ♥ por Bárbara Erick e Lucas dos Santos</p>
            </div>
        </footer>

        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/scripts.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    </body>

</html>

