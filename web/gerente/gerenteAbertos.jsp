<%-- 
    Document   : gerenteAbertos
    Created on : 28/09/2019, 16:25:15
    Author     : lucas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="robots" content="index, follow"/>


        <title>Embuste</title>

        <!-- Fav icon -->


        <!-- Simbols -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Style -->
		<link rel="stylesheet" href="../css/reset.css">
		<link rel="stylesheet" href="../css/styles.css">

    </head>

    <body>
         <div class="pano-menu-mobile"></div>

        <nav id="nav1" class="engloba-nav-bar transition">
            <div class="logo">
                <img alt="pay now" src="../img/logo.png">
            </div>
            <ul class="nav-itens">
                <li>
                    <a class="nav-item nav-active" href="http://localhost:8080/TrabalhoWeb2/gerente/gerenteInicio.jsp">
                        Inicio
                    </a>
                </li>
                <li >
    	            <a class="nav-item" href="http://localhost:8080/TrabalhoWeb2/gerente/gerenteAtendimentos.jsp">
    	               Atendimentos
    	            </a>
                </li>
                <li >
    	            <a class="nav-item" href="http://localhost:8080/TrabalhoWeb2/gerente/gerenteUsuarios.jsp">
    	               Usuários
    	            </a>
                </li>
                <li >
    	            <a class="nav-item" href="http://localhost:8080/TrabalhoWeb2/gerente/gerenteAbertos.jsp">
    	               Abertos
    	            </a>
                </li>
                <li >
                    <a class="nav-item" href="#">
                       Logout
                    </a>
                </li>
            </ul>
        </nav>

        <header  class="top-header transition" >
            <div class="logo">
                <img alt="pay now" src="../img/logo.png">
            </div>
            <div class="button-menu">
                <i class="open fas fa-bars"></i>
                <i class="close fas fa-times"></i>
            </div>
        </header>

        <section class="section-banner">
            <div class="container">
                <div class="banner">
                    <h1 class="title-usuario">Atendimentos abertos </h1>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="tabela">
                    <table class="table table-sm">
                      <thead>
                        <tr>
                          <th class="destaque" scope="col">Id</th>
                          <th class="destaque" scope="col">Tipo de atendimento</th>
                          <th class="destaque" scope="col">Cliente</th>
                          <th class="destaque" scope="col">Data</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>2</td>
                          <td>Reclamação</td>
                          <td>jessica</td>
                          <td>4/05/19</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Sugestão</td>
                          <td>joao</td>
                          <td>15</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Blabla</td>
                          <td>pedro</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Blabla</td>
                          <td>pedro</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Blabla</td>
                          <td>pedro</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Blabla</td>
                          <td>pedro</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Blabla</td>
                          <td>pedro</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Blabla</td>
                          <td>pedro</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Blabla</td>
                          <td>pedro</td>
                          <td>100</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>
        </section>


        <footer class="footer">
            <div class="container">
                <img alt="embuste" src="../img/logo.png">
                <p>Criado com ♥ por Bárbara Erick e Lucas dos Santos</p>
            </div>
        </footer>

        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/scripts.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    </body>

</html>

